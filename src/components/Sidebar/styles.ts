import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
  pokemonListItem: {
    listStyleType: 'none',
    margin: 24,
    marginLeft: 0,
    fontSize: 18
  },
  pokemonList: {
    padding: 0
  },
  pokemonListLink: {
    textDecoration: 'none',
    color: '#fff',
    cursor: 'pointer'
  },
  sidebarContainer: {
    background: 'none repeat scroll 0 0 #0099ff',
    borderRadius: 24,
    padding: 24,
    fontFamily: 'Open Sans',
    height: 'fit-content'
  }
});

export default styles;
