import React from 'react';
import { shallow } from 'enzyme';
import { StyleSheetTestUtils } from 'aphrodite';

import Sidebar from '../';

describe('<Sidebar />', () => {
  beforeEach(() => {
    StyleSheetTestUtils.suppressStyleInjection();
  });
  afterEach(() => {
    StyleSheetTestUtils.clearBufferAndResumeStyleInjection();
  });

  it('Should render component', () => {
    let setSelectedPokemon;
    beforeEach(() => {
      setSelectedPokemon = jest.fn();
    });

    const wrapper = shallow(
      <Sidebar pokemonList={[]} setSelectedPokemon={setSelectedPokemon} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
