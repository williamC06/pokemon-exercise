import React from "react";
import { css } from "aphrodite";

import { PokemonInterface } from "../../types/common";

import styles from "./styles";

interface SidebarPropsInterface {
  pokemonList: PokemonInterface[];
  setSelectedPokemon: (pokemonUrl: string) => void;
}

const Sidebar: React.SFC<SidebarPropsInterface> = ({
  pokemonList,
  setSelectedPokemon
}) => {
  const renderPokemonList = () =>
    pokemonList.map((pokemon: PokemonInterface) => (
      <li
        className={css(styles.pokemonListItem)}
        key={pokemon.name}
        onClick={() => setSelectedPokemon(pokemon.url)}
      >
        <a className={css(styles.pokemonListLink)}>
          {pokemon.name}
        </a>
      </li>
    ));

  return (
    <div className={css(styles.sidebarContainer)}>
      <h2>Pokemon List</h2>
      <ul className={css(styles.pokemonList)}>{renderPokemonList()}</ul>
    </div>
  );
};

export default Sidebar;
