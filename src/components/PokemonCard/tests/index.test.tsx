import React from 'react';
import { shallow } from 'enzyme';
import { StyleSheetTestUtils } from 'aphrodite';

import PokemonCard from '../';

describe('<PokemonCard />', () => {
  beforeEach(() => {
    StyleSheetTestUtils.suppressStyleInjection();
  });
  afterEach(() => {
    StyleSheetTestUtils.clearBufferAndResumeStyleInjection();
  });
  const selectedPokemon = {
    sprites: {
      front_default:
        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/4.png'
    },
    abilities: [],
    moves: []
  };

  it('Should render component', () => {
    const wrapper = shallow(<PokemonCard selectedPokemon={selectedPokemon} />);
    expect(wrapper).toMatchSnapshot();
  });
});
