import React from 'react';
import { css } from 'aphrodite';

import styles from './styles';
import {
  PokemonAbilityInterface,
  PokemonMoveInterface
} from '../../types/common';

interface PokemonCardInterface {
  selectedPokemon: any;
}

const PokemonCard: React.FC<PokemonCardInterface> = ({ selectedPokemon }) => {
  const renderPokemonAbilities = (abilities: PokemonAbilityInterface[]) =>
    abilities.map((ability: PokemonAbilityInterface) => (
      <li key={ability.ability.name}>{ability.ability.name}</li>
    ));

  const renderPokemonMoves = (moves: PokemonMoveInterface[]) =>
    moves.map((move: PokemonMoveInterface) => (
      <li key={move.move.name}>{move.move.name}</li>
    ));

  const renderPokemonCard = () =>
    selectedPokemon ? (
      <div className={css(styles.pokemonCardContainer)}>
        <div>
          <img
            alt={`front ${selectedPokemon.name}`}
            src={selectedPokemon.sprites.front_default}
          />
          <img
            alt={`back ${selectedPokemon.name}`}
            src={selectedPokemon.sprites.back_default}
          />
        </div>
        <ul>
          <li>Id: {selectedPokemon.id}</li>
          <li>Name: {selectedPokemon.name}</li>
          <li>Height: {selectedPokemon.height}</li>
          <li>Weight: {selectedPokemon.weight}</li>
          <li>Base Experience: {selectedPokemon.base_experience}</li>
          <li>
            Abilities:
            <ul>{renderPokemonAbilities(selectedPokemon.abilities)}</ul>
          </li>
          <li>
            Moves:
            <ul>{renderPokemonMoves(selectedPokemon.moves)}</ul>
          </li>
        </ul>
      </div>
    ) : null;

  return (
    <div className={css(styles.pokemonCardWrapper)}>{renderPokemonCard()}</div>
  );
};

export default PokemonCard;
