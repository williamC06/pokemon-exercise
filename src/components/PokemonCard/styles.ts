import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
  pokemonCardWrapper: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center'
  },
  pokemonCardContainer: {
    width: 'fit-content',
    fontFamily: 'Open Sans',
    fontSize: 18,
    border: '1px solid #0099ff',
    borderRadius: 24,
    padding: 64
  }
});

export default styles;
