export interface PokemonInterface {
  name: string;
  url: string;
}

export interface PokemonAbilityInterface {
  is_hidden: boolean;
  slot: number;
  ability: {
    name: string;
    url: string
  }
}

export interface PokemonMoveInterface {
  move: {
    name: string;
    url: string;
  }
  version_group_details?: Object
}

export interface RootReducerStateInterface {
  pokemons: PokemonInterface[];
  selectedPokemon: any;
  err: string;
}
