import axios from "axios";

export const fetchPokemonsData = async () => {
  try {
    const res = await axios.get("https://pokeapi.co/api/v2/pokemon");
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const fetchIndividualPokemonData = async (
  selectedPokemonUrl: string
) => {
  try {
    const res = await axios.get(selectedPokemonUrl);
    return res;
  } catch (err) {
    console.log(err);
  }
};
