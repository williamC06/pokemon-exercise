import { takeLatest, call, put } from 'redux-saga/effects';

import { appActionTypes } from './constants';
import {
  fetchPokemonsData,
  fetchIndividualPokemonData
} from '../../services/api';
import {
  getPokemonsFailure,
  getPokemonsSuccess,
  setSelectedPokemonSuccess,
  setSelectedPokemonFailure
} from './actions';

export function* getPokemonsSaga() {
  try {
    const res = yield call(fetchPokemonsData);
    const { data } = res;
    yield put(getPokemonsSuccess(data.results));
  } catch (err) {
    yield put(getPokemonsFailure(err));
  }
}

export function* setSelectedPokemonSaga(action) {
  try {
    const res = yield call(
      fetchIndividualPokemonData,
      action.payload.selectedPokemonUrl
    );
    const { data } = res;
    yield put(setSelectedPokemonSuccess(data));
  } catch (err) {
    yield put(setSelectedPokemonFailure(err));
  }
}

export default function* appSaga() {
  yield takeLatest(appActionTypes.GET_POKEMONS_REQUEST, getPokemonsSaga);
  yield takeLatest(
    appActionTypes.SET_SELECTED_POKEMON_REQUEST,
    setSelectedPokemonSaga
  );
}
