import { call, put } from 'redux-saga/effects';

import { getPokemonsSaga, setSelectedPokemonSaga } from '../saga';
import { getPokemonsSuccess, setSelectedPokemonSuccess } from '../actions';
import {
  fetchPokemonsData,
  fetchIndividualPokemonData
} from '../../../services/api';
import { appActionTypes } from '../constants';

describe('App container Saga', () => {
  it('handles getPokemonSaga', async () => {
    const gen = getPokemonsSaga();

    expect(gen.next().value).toEqual(call(fetchPokemonsData));

    expect(gen.next({ data: { results: [] } }).value).toEqual(
      put(getPokemonsSuccess([]))
    );

    expect(gen.next().done).toBeTruthy;
  });

  it('handles setSelectedPokemonSaga', async () => {
    const selectedPokemonUrl = 'https://pokeapi.co/api/v2/pokemon/3/';

    const gen = setSelectedPokemonSaga({
      type: appActionTypes.SET_SELECTED_POKEMON_REQUEST,
      payload: {
        selectedPokemonUrl
      }
    });

    expect(gen.next().value).toEqual(
      call(fetchIndividualPokemonData, selectedPokemonUrl)
    );

    expect(gen.next({ data: {} }).value).toEqual(
      put(setSelectedPokemonSuccess({}))
    );

    expect(gen.next().done).toBeTruthy;
  });
});
