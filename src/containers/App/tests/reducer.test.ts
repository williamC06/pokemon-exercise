import { rootReducerInitialState } from './../reducer';
import { appActionTypes } from './../constants';
import rootReducer from '../reducer';

describe('App rootReducer', () => {
  let state;

  beforeEach(() => {
    state = rootReducerInitialState;
  });

  it('Should handle GET_POKEMONS_SUCCESS', () => {
    expect(
      rootReducer(state, {
        type: appActionTypes.GET_POKEMONS_SUCCESS,
        payload: {
          pokemons: []
        }
      })
    ).toEqual({ ...state, pokemons: [] });
  });

  it('Should handle GET_POKEMONS_FAILURE', () => {
    expect(
      rootReducer(state, {
        type: appActionTypes.GET_POKEMONS_FAILURE,
        payload: {
          err: 'An error has ocurred'
        }
      })
    ).toEqual({ ...state, err: 'An error has ocurred' });
  });

  it('Should handle SET_SELECTED_POKEMON_SUCCESS', () => {
    expect(
      rootReducer(state, {
        type: appActionTypes.SET_SELECTED_POKEMON_SUCCESS,
        payload: {
          pokemon: {}
        }
      })
    ).toEqual({ ...state, selectedPokemon: {} });
  });

  it('Should handle SET_SELECTED_POKEMON_FAILURE', () => {
    expect(
      rootReducer(state, {
        type: appActionTypes.SET_SELECTED_POKEMON_FAILURE,
        payload: {
          err: 'Error selecting pokemon'
        }
      })
    ).toEqual({ ...state, err: 'Error selecting pokemon' });
  });
});
