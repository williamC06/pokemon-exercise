import {
  getPokemonsSuccess,
  getPokemonsFailure,
  setSelectedPokemonSuccess,
  setSelectedPokemonFailure
} from './../actions';
import { appActionTypes } from './../constants';

describe('App container actions', () => {
  it('has a type of GET_POKEMONS_SUCCESS', () => {
    const expected = {
      type: appActionTypes.GET_POKEMONS_SUCCESS,
      payload: {
        pokemons: []
      }
    };
    expect(getPokemonsSuccess([])).toEqual(expected);
  });

  it('has a type of GET_POKEMONS_FAILURE', () => {
    const expected = {
      type: appActionTypes.GET_POKEMONS_FAILURE,
      payload: {
        err: 'error here'
      }
    };
    expect(getPokemonsFailure('error here')).toEqual(expected);
  });

  it('has a type of SET_SELECTED_POKEMON_SUCCESS', () => {
    const expected = {
      type: appActionTypes.SET_SELECTED_POKEMON_SUCCESS,
      payload: {
        pokemon: {}
      }
    };
    expect(setSelectedPokemonSuccess({})).toEqual(expected);
  });

  it('has a type of SET_SELECTED_POKEMON_FAILURE', () => {
    const expected = {
      type: appActionTypes.SET_SELECTED_POKEMON_FAILURE,
      payload: {
        err: 'error here'
      }
    };
    expect(setSelectedPokemonFailure('error here')).toEqual(expected);
  });
});
