import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { shallow } from 'enzyme';

import App from '../';
import rootReducer from '../reducer';

describe('<App />', () => {
  const store = createStore(rootReducer);

  it('Should render component', () => {
    const wrapper = shallow(
      <Provider store={store}>
        <App />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
