import { appActionTypes } from './constants';
import { RootReducerStateInterface } from '../../types/common';

export const rootReducerInitialState: RootReducerStateInterface = {
  pokemons: [],
  selectedPokemon: null,
  err: ''
};

const rootReducer = (
  state: RootReducerStateInterface = rootReducerInitialState,
  action: {
    type: string;
    payload?: any;
  }
) => {
  const { payload } = action;

  switch (action.type) {
    case appActionTypes.GET_POKEMONS_SUCCESS:
      return { ...state, pokemons: payload.pokemons };

    case appActionTypes.GET_POKEMONS_FAILURE:
      return { ...state, err: payload.err };

    case appActionTypes.SET_SELECTED_POKEMON_SUCCESS:
      return { ...state, selectedPokemon: payload.pokemon };

    case appActionTypes.SET_SELECTED_POKEMON_FAILURE:
      return { ...state, err: payload.err };

    default:
      return state;
  }
};

export default rootReducer;
