import { appActionTypes } from "./constants";
import { PokemonInterface } from "../../types/common";

export const getPokemonsRequest = () => ({
  type: appActionTypes.GET_POKEMONS_REQUEST
});

export const getPokemonsSuccess = (pokemons: PokemonInterface[]) => ({
  type: appActionTypes.GET_POKEMONS_SUCCESS,
  payload: {
    pokemons
  }
});

export const getPokemonsFailure = (err: string) => ({
  type: appActionTypes.GET_POKEMONS_FAILURE,
  payload: {
    err
  }
});

export const setSelectedPokemonRequest = (selectedPokemonUrl: string) => ({
  type: appActionTypes.SET_SELECTED_POKEMON_REQUEST,
  payload: {
    selectedPokemonUrl
  }
});

export const setSelectedPokemonSuccess = pokemon => ({
  type: appActionTypes.SET_SELECTED_POKEMON_SUCCESS,
  payload: {
    pokemon
  }
});

export const setSelectedPokemonFailure = (err: string) => ({
  type: appActionTypes.SET_SELECTED_POKEMON_FAILURE,
  payload: {
    err
  }
});
