import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { css } from 'aphrodite';

import { getPokemonsRequest, setSelectedPokemonRequest } from './actions';

import Sidebar from '../../components/Sidebar';
import PokemonCard from '../../components/PokemonCard';

import { RootReducerStateInterface } from '../../types/common';

import styles from './styles';

const App: React.FC = () => {
  const pokemonList = useSelector(
    (state: RootReducerStateInterface) => state.pokemons
  );
  const selectedPokemon = useSelector(
    (state: RootReducerStateInterface) => state.selectedPokemon
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPokemonsRequest());
  }, []);

  const handleSetSelectedPokemon = (url: string) =>
    dispatch(setSelectedPokemonRequest(url));

  return (
    <div className={css(styles.appWrapper)}>
      <Sidebar
        pokemonList={pokemonList}
        setSelectedPokemon={handleSetSelectedPokemon}
      />
      <PokemonCard selectedPokemon={selectedPokemon} />
    </div>
  );
};

export default App;
