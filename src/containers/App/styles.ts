import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
  appWrapper: {
    display: 'flex',
  }
});

export default styles;
