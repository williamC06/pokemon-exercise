# Pokemon Exercise

## Installation

- Clone the repository: `https://gitlab.com/williamC06/pokemon-exercise.git`.
- Run `npm i` or `yarn add` to install dependencies.
- Run `npm start` or `yarn start` to start the application.

## Available Scripts

In the project directory, you can run:

#### `yarn start`

Runs the application in `localhost:3000`.

#### `yarn test`

Starts execution of tests.

## Possible improvements

Due to time/effort constraints, there are several areas that could be improved:

- **More rigorous testing** - Testing positive and negative outputs, garbage inputs, component unit testing ,etc.
- **Styling** - Make the application a little bit nicer to look at. Fix inconsistencies in the CSS code, etc.
- **Typing** - Stricter typing, especially in api calls and data structures.
- **Error Handling** - Improve error handling in the application. Probably some sort of notification popup for the user, and better handling for developers, instead of just doing a `console.log`.
- **Pokemon Card Image Loading** - There's a slight lag when loading images after changing pokemons to display. We can probably just add an `onLoad` prop to the image and wait for all the data to load before displaying it.

## Design Decisions

- Considering the project is using `react-redux`'s new hooks, it might not be necessary to separate components as `components` and `containers`. Provided hooks make it a lot easier to access the redux store. It was done purely for organization purposes.
